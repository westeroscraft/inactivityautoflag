package me.whatshiywl.iaf;

import java.io.File;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PluginListener implements Listener{

	private InactivityAutoFlag plugin;

	public PluginListener(final InactivityAutoFlag plugin){
		this.plugin = plugin;
	}

	@EventHandler
	public void onPlayerJoinEvent(PlayerJoinEvent event){
		if(!InactivityAutoFlag.trackonlogin)return;
		if(!InactivityAutoFlag.groupmanager)return;
		Player player = event.getPlayer();
		trackPlayer(player);
	}

	@EventHandler
	public void onPlayerQuitEvent(PlayerQuitEvent event){
		if(InactivityAutoFlag.trackonlogin)return;
		if(!InactivityAutoFlag.groupmanager)return;
		Player player = event.getPlayer();
		trackPlayer(player);
	}

	public void trackPlayer(Player player){
		InactivityAutoFlag.log.info("Registering " + player.getName() + "'s activity!");
		FileConfiguration players = plugin.getPlayers();
		if(!players.contains(player.getName())){
			InactivityAutoFlag.log.info("Creating new tracker for player " + player.getName());
			players.createSection(player.getName());
		}
		players.set(player.getName(), System.currentTimeMillis());
		try {
			players.save(new File(plugin.getDataFolder(), "players.yml"));
		} catch (Exception e) {
			InactivityAutoFlag.log.severe("Failed to track " + player.getName() + ": " + e);
		}
	}
}